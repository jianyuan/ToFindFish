// miniprogram/pages/seller/seller.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tempFilePaths:[],//选择的图片
    name:'',//商品名称
    price:'',//价格
    size:'',//规格
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  onChoiceImage:function(e){
    var that = this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        console.log(res);
        // tempFilePath可以作为img标签的src属性显示图片
        that.setData({tempFilePaths:res.tempFilePaths});

      }
    })
  },
  nameInputChange:function(e){
    this.setData({
      name: e.detail.value
    })
  },
  priceInputChange: function (e) {
    this.setData({
      price: e.detail.value
    })
  },
  sizeInputChange: function (e) {
    this.setData({
      size: e.detail.value
    })
  },
  funSubmit:function(e){
    console.log(this.data.name, this.data.price, this.data.size);
    
  }
})